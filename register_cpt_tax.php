<?php

// =============================
// custom taxonomies (cats and tags)
// =============================

// no direct access!
defined('ABSPATH') or die("No script kiddies please!");


function register_cpt_tax() {

    global $wsc;
    $name = ( isset( $wsc['cpt-label']) ) ? $wsc['cpt-label'] : 'JDA';

    $labels = array(
        'name'                => _x( $name, 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( $name, 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( $name, 'text_domain' ),
    );
    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'revisions', 'custom-fields', 'post-formats', 'author', 'thumbnail' ),
        'taxonomies'          => array( 'ifcc_cats', 'ifcc_tags' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'menu_position'       => 20,
        'has_archive'         => true,
        'capability_type'     => 'ifcc',
    'capabilities'        => array( 'read_ifcc', 'edit_ifcc', 'delete_ifcc' ),
    'map_meta_cap'        => true
    );  
    
    // register cpt ifcc
    register_post_type( 'ifcc', $args );

    // define tax cat
    $labels = array(
        'name'                       => _x( $name . ' Categories', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( $name . ' Category', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( $name . ' category', 'text_domain' ),
        'all_items'                  => __( 'All Items', 'text_domain' ),
        'parent_item'                => __( 'Parent Item', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
        'new_item_name'              => __( 'New Item Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Item', 'text_domain' ),
        'edit_item'                  => __( 'Edit Item', 'text_domain' ),
        'update_item'                => __( 'Update Item', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
        'search_items'               => __( 'Search Items', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => array( 'slug' => 'ifcc-categories' ),
        'query_var'                  => true
    );
    register_taxonomy( 'ifcc_cats', array( 'ifcc' ), $args );

    // define tax tag
    $labels = array(
        'name'                       => _x( $name . ' Tags', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( $name . ' Tag', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( $name . ' Tag', 'text_domain' ),
        'all_items'                  => __( 'All Items', 'text_domain' ),
        'parent_item'                => __( 'Parent Item', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
        'new_item_name'              => __( 'New Item Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Item', 'text_domain' ),
        'edit_item'                  => __( 'Edit Item', 'text_domain' ),
        'update_item'                => __( 'Update Item', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
        'search_items'               => __( 'Search Items', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => array( 'slug' => 'ifcc-tags' ),
        'query_var'                  => true
    );
    register_taxonomy( 'ifcc_tags', array( 'ifcc' ), $args );

    // explicit link cpt <==> tax
    register_taxonomy_for_object_type( 'ifcc_cats', 'ifcc' );
    register_taxonomy_for_object_type( 'ifcc_tags', 'ifcc' );

    // =============================
    // fix admin and woo user perms
    // =============================
    $role = get_role('administrator');
    if( ! $role->has_cap('edit_ifcc') ){
        $role->add_cap( 'edit_ifcc' );
        $role->add_cap( 'read_ifcc' );
        $role->add_cap( 'delete_ifcc' );
        $role->add_cap( 'delete_ifccs' );
        $role->add_cap( 'edit_ifccs' );
        $role->add_cap( 'edit_others_ifccs' );
        $role->add_cap( 'publish_ifccs' );
        $role->add_cap( 'read_private_ifccs' );
        $role->add_cap( 'delete_private_ifccs' );
        $role->add_cap( 'delete_published_ifccs' );
        $role->add_cap( 'delete_others_ifccs' );
        $role->add_cap( 'edit_private_ifccs' );
        $role->add_cap( 'edit_published_ifccs' );            
    }

    if( null == add_role( 'customer', 'customer' ) ){
        $role = get_role('customer');
        if( ! $role->has_cap( 'edit_ifcc' ) ){
            $role->add_cap( 'edit_ifcc' );
            $role->add_cap( 'edit_ifccs' );
            $role->add_cap( 'delete_ifcc' );
            $role->add_cap( 'delete_ifccs' );
            $role->add_cap( 'edit_published_ifccs' );    
            $role->add_cap( 'publish_ifccs' );
            $role->add_cap( 'read_ifcc' );
            $role->add_cap( 'upload_files' );
        }
    }

    if( isset( $wsc['block-dashboard']) && $wsc['block-dashboard'] ){
        // block non admin users from dashboard!
        if ( is_admin() && ! current_user_can( 'administrator' ) && 
           ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
            wp_redirect( home_url() );
            exit;
        }
    }

}




/**
 * Add Columns to ifcc Edit Screen
 * http://wptheming.com/2010/07/column-edit-pages/
 */

function ifcc_edit_columns( $columns ) {
    $columns['ifcc_ratings']  = __( 'Post Rating', 'text_domain' );
    $columns['ifcc_views']  = __( 'Post Views', 'text_domain' );
    $columns['thumbnail']  = __( 'Thumb', 'text_domain' );
    return $columns;
}

function ifcc_column_display( $column, $post_id ) {

    // Code from: http://wpengineer.com/display-post-thumbnail-post-page-overview

    switch ( $column ) {
            
            // Display the rating position
            case "ifcc_ratings":
                if ( $ifcc_ratings = get_post_meta( get_the_ID(), 'ifcc_ratings', true ) ) {
                    echo $ifcc_ratings;
                } else {
                    echo '&mdash;';
                }
            break;

            // Display the rating position
            case "ifcc_views":
                if ( $ifcc_views = get_post_meta( get_the_ID(), 'ifcc_views', true ) ) {
                    echo $ifcc_views;
                } else {
                    echo '&mdash;';
                }
            break;

            // Display the thumbnail
            case "thumbnail":
                if ( has_post_thumbnail( get_the_ID() ) ) {
                    the_post_thumbnail( array(50, 50) );
                } else {
                    echo '&mdash;';
                }
            break;

            // Display the ifcc tags in the column view
            case "ifcc_category":
                if ( $category_list = get_the_term_list( $post_id, 'ifcc_cats', '', ', ', '' ) ) {
                    echo $category_list;
                } else {
                    echo '&mdash;';
                }
            break;

            // Display the ifcc tags in the column view
            case "ifcc_tag":
                if ( $tag_list = get_the_term_list( $post_id, 'ifcc_tags', '', ', ', '' ) ) {
                    echo $tag_list;
                } else {
                    echo '&mdash;';
                }
            break;
    }
}

/**
 * Adds taxonomy filters to the ifcc admin page
 * Code artfully lifted from http://pippinsplugins.com
 */

function ifcc_add_taxonomy_filters() {
    global $typenow;
    global $wsc;
    if( ! $wsc['show-tax-filters'] ) return;  

    // An array of all the taxonomyies you want to display. Use the taxonomy name or slug
    $taxonomies = array( 'ifcc_cats', 'ifcc_tags' );

    // must set this to the post type you want the filter(s) displayed on
    if ( 'ifcc' == $typenow ) {

        foreach ( $taxonomies as $tax_slug ) {
            $current_tax_slug   = isset( $_GET[$tax_slug] ) ? $_GET[$tax_slug] : false;
            $tax_obj            = get_taxonomy( $tax_slug );
            $tax_name           = $tax_obj->labels->name;
            $terms              = get_terms( $tax_slug );
            if ( count( $terms ) > 0) {
                echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
                echo "<option value=''>$tax_name</option>";
                foreach ( $terms as $term ) {
                    echo '<option value=' . $term->slug, $current_tax_slug == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
                }
                echo "</select>";
            }
        }
    }
}

// Make rating column sortable
function sortable_columns() {
    return array(
        'ifcc_ratings' => 'ifcc_ratings',
        'ifcc_views' => 'ifcc_views',
        'title' => 'title',
        'date'  => 'date'
    );
}


function ifcc_ratings( $vars ) {
    // check if we are on our post type
    if ( isset( $vars['post_type'] ) && 'ifcc' == $vars['post_type'] ) {
        // Check if 'orderby' is set to 'ratings position'
        if ( isset( $vars['orderby'] ) && 'ifcc_ratings' == $vars['orderby'] ) {
            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars,
                array(
                    'meta_key' => 'ifcc_ratings',
                    'orderby' => 'meta_value_num'
                )
            );
        } else if( isset( $vars['orderby'] ) && 'ifcc_views' == $vars['orderby'] ){
            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars,
                array(
                    'meta_key' => 'ifcc_views',
                    'orderby' => 'meta_value_num'
                )
            );            
        }
    }
    return $vars;
}