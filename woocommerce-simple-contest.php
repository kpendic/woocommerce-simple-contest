<?php 
/*
Plugin Name: WooCommerce Simple contest plugin
Plugin URI: http://mkdizajn.github.io
Description: Simple contest plugin does what? It gives you power to run simple constest where people can buy licence to submit their work to be publicly viewable and rateable by other folks.. Two required plugins are needed to run the contest: ACF & WOOCOMMERCE
Version: 0.9
Author: krex
Author URI: http://mkdizajn.github.io
Author Email: kpendic@gmail.com
License:

    WooCommerce simple contest plugin does what,, it gives you power to run
    simple constest where people can buy licence to submit 
    their work to be publicly viewable and rateable by other folks.. 
    Two required plugins are needed to run the contest: ACF & WOOCOMMERCE
    Copyright (C) 2014 Kresimir Pendic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// no direct access!
defined('ABSPATH') or die("No script kiddies please!");

// Registers the Post Series Taxonomy
if ( ! class_exists( 'woosimplecontest' ) ) {
    class woosimplecontest {

        /**
         * brrm, brrrmm -- let's go :)
         */
        public function __construct() {
            global $wsc;

            // register cpt and tax
            require_once dirname( __FILE__ ) . '/register_cpt_tax.php' ;
            // include settings page 
            require_once dirname( __FILE__ ) . '/admin/admin-init.php' ;
            // include required plugins!
            require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php' ;
            // required plugins ..
            add_action( 'tgmpa_register', array( $this, 'my_plugin_register_required_plugins' ) );
            // register contest ctp & tax then fix user acl
            add_action( 'init', 'register_cpt_tax', 1, 1 );
            // refresh rewrite rules
            register_activation_hook( __FILE__, array( $this, 'activate' ) );
            // set columns in admin
            add_filter( 'manage_edit-ifcc_columns', 'ifcc_edit_columns' );
            // Adds columns in the admin view for taxonomies
            add_action( 'manage_ifcc_posts_custom_column', 'ifcc_column_display', 10, 2 );
            // Add thumbnail, ratings columns
            add_filter( 'manage_edit-ifcc_sortable_columns', 'sortable_columns' );
            // rating -> sorting!
            add_action( 'load-edit.php', create_function( '' , 'add_filter( "request", "ifcc_ratings" );' ) );
            // Allows filtering of posts by taxonomy in the admin view
            add_action( 'restrict_manage_posts', 'ifcc_add_taxonomy_filters' );
            // in my account add ilst of submitions for each user
            add_action( 'woocommerce_before_my_account', array( $this, 'myaccountifcc') );
            // after buying the licence update user profile
            add_action( 'woocommerce_thankyou', array( $this, 'checklicence') );
            // main contest logic
            add_action( 'template_redirect' , array( $this, 'cpt_front') );
            // do something to our cpt post after save ,, set post format, set featured image etc..
            add_action( 'acf/save_post',  array( $this, 'savefi' ), 20, 1 );
            // add filter for categories in sidebar** todo.. maybe to generate new sidebar only for contest??
            add_action( 'get_sidebar', array( $this, 'firstinsidebar'), 10 );
            // add post formats 
            add_action( 'after_setup_theme', array( $this, 'childtheme_formats' ) , 11 );
            // add rating|views counter
            add_action( 'wp_head', array( $this, 'metacounter' ) );
            add_action( 'wp_print_styles', array( $this, 'my_deregister_styles') , 100 );
            // add number of queries in html to diagnose this plugin
            add_action( 'wp_footer', array( $this, 'querycounter' ), 100 );
        }

        public function querycounter(){
            echo '<!-- after wsc: '. get_num_queries() . ' queries, in time: '; 
            timer_stop(1);
            echo ' sec. -->' ; 
        }

        public function my_deregister_styles() {
            global $wsc;
            if( $wsc['remove-admin-css'] ){
                wp_deregister_style( 'wp-admin' );
                wp_deregister_style( 'acf' );
            }
        }

        // =============================
        // after plugin activates flush rewrite rules! == only once!
        // =============================
        public function activate() {
            flush_rewrite_rules();
        }

        // add support for post formats!
        public function childtheme_formats(){
            // add_theme_support( 'post-formats', array( 'aside', 'gallery', 'quote', 'video', 'audio' ) );
            // remove_theme_support( 'post-formats' );
            add_post_type_support( 'ifcc', 'post-formats' );
        }

        // =============================
        // post view on frontend increase counter
        // =============================
        public function metacounter(){
            global $post; // get that post!
            if ( ! is_admin() ){ // we are on frontend not admin!
                if ( is_single( $post ) && $post->post_type == 'ifcc' ){ // only trigger on single and target post type
                    $views = get_post_meta( $post->ID , 'ifcc_views', true); // old value
                    update_post_meta( $post->ID , 'ifcc_views', $views+1 ); // new value
                }
            }            
        }

        // =============================
        // my account -> list user's submitions
        // =============================
        public function myaccountifcc(){
            global $wsc;
            if( ! $wsc[ 'show-in-myaccount' ] ) return;
            $ifccs = new WP_Query( array(
                'post_type'     => 'ifcc',
                'post_status'   => array( 'draft', 'publish' ),
                'author'        => get_current_user_id()
            ));

            if(  $ifccs->have_posts() ){ // todo: have to extend output. need to add edit sub, remove etc..
                echo "<h2>Your contest submitions</h2>";
                echo "<dd>";
                while ( $ifccs->have_posts() ) : $ifccs->the_post();
                    echo '<dt><a href="' . get_permalink() . '">';
                    the_title();
                    echo '</a></dt>';
                endwhile;
                echo "</dd>";
            }
            wp_reset_query();
        }


        // =============================
        // after order received, check and add user a licence
        // =============================
        public function checklicence( $order_id ) {
            global $woocommerce;
            global $wsc;

            $order = new WC_Order($order_id);
            $user_id = get_current_user_id();

            foreach($order->get_items() as $item){
                if( $wsc['ifcc-product'] == $item['product_id'] ){
                    update_user_meta( $user_id, $wsc['user-meta-licence'], '0' );
                    wp_redirect( get_permalink( $wsc['ifcc-landing-page'] ) );
                } 
            }
        }


        // =============================
        // main redirect logic 
        // =============================
        public function cpt_front($redirect) {
            global $post;
            global $wsc; // settings object

            if( is_admin() || null == $post ) return; // bail if on admin or no post obj
            // bail early if no settings are set yet
            if ( ! isset( $wsc['ifcc-landing-page'] ) ) return;
            // do not fall in redirect loop trick :)
            if( $post->ID == get_option('woocommerce_myaccount_page_id') ) return;

            if ( $post->post_type == 'ifcc' ) {
                // can anonimous users see contest archive page?
                if( $wsc['anonim-see-contest'] ) return;
                if( is_user_logged_in() ){
                    if( ! $wsc['admin-see-contest'] && ! current_user_can( 'administrator' )) wp_redirect( home_url() );
                    // on arhive page (inherit theme's layout) ..  todo (embed custom templete??)
                    if ( is_archive() ) return;
                    // on single submition show fields of content
                    if ( is_single() ) add_action( 'the_content', array( &$this,  'showsall' ), 15 );
                } else {
                    wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); 
                }
            }

            // on contest page .. show form
            if( $post->ID == $wsc['ifcc-landing-page'] ){
                acf_form_head();
                add_action( 'the_content', array( &$this, 'callsall' ), 20 );
            }

        }

        // =============================
        // show the acf custom form => submit page
        // =============================
        public function callsall($content){
            // bails
            if( is_admin() || ! is_user_logged_in() ) return $content;

            global $wsc;
            // get that user
            $user_id = get_current_user_id();
            // bail if user licence is not ok
            $free = get_user_meta( $user_id, $wsc['user-meta-licence'], true );
            if( $free == '1' ) return $wsc['thankyou-note'];

            $w = new WP_Query( array( 'post_type'=>'ifcc', 'post_status'=>array( 'trash' ), 'author'=>$user_id ));

            // second time -> check if user did created (trashed) post and resume on it :)~
            // I opted to trash post so it does not make cluter inside posts screen..
            if( $w->have_posts() ){
                $w->the_post();
                $post_id = get_the_ID();
            } else {
                // first time insert the post
                $post = array(
                    'post_status'   => 'trash' ,
                    'post_title'    => 'contest',
                    'post_author'   => $user_id,
                    'post_type'     => 'ifcc',
                );
                $post_id = wp_insert_post( $post );
                add_post_meta( $post_id, 'ifcc_ratings', 1 ); // rating counter
                add_post_meta( $post_id, 'ifcc_views', 1 );   // views  counter
            }


            $fg = ( isset( $wsc['ifcc-fields'] ) ) ? $wsc['ifcc-fields'] : '';
            $args = array(
                'field_groups'      => array( $fg ),
                'post_id'           => $post_id,
                'submit_value'      => 'Submit your work',
            );
            // show the form on page
            return $fg;
            // return acf_form( $args );
        }


        // =============================
        // save image as featured image, post formats
        // =============================
        public function savefi( $post_id ){
            // bail if not on frontend == carefull!
            if( is_admin() ) return;
            global $wsc;
            $fields = get_fields( $post_id );

            // trash => draft
            $work_title = $fields['work_title'];
            $post = array(
                'post_status' => 'draft' ,
                'post_title'  => $work_title,
                'post_name'   => $work_title,
                'ID'          => $post_id,
            );
            wp_update_post( $post );
            $user_id = get_current_user_id();
            // user lower licence down
            update_user_meta( $user_id, $wsc['user-meta-licence'], '1' );

            // formats, thumb
            if( $fields ){
                foreach( $fields as $field_name => $value ){
                    $f = get_field_object($field_name, false, array('load_value' => false));
                    if( stristr( $f['name'], 'picture' )!==false||stristr( $f['name'], 'image' )!==false ) {
                        if( $value['id'] != '' ) set_post_thumbnail( $post_id, $value['id'] );

                    }
                    // automatic post formats do not show thumbnails on posts.. in most themes?
                    // elseif ( stristr( $f['name'], 'video' )!==false||stristr( $f['name'], 'movie' )!==false ) {
                    //     if( $value != '' ) set_post_format( $post_id, 'video' );

                    // } elseif ( stristr( $f['name'], 'audio' )!==false||stristr( $f['name'], 'sound' )!==false ) {
                    //     if( $value != '' ) set_post_format( $post_id, 'audio' );
                    // }
                }
            }

            // FORMAT GALLERY
            $attachments = get_children(
                array(
                    'post_type' => 'attachment',
                    'post_mime_type' => 'image',
                    'post_parent' => $post_id
                )
            );
            // (count($attachments) > 1 && !get_post_format( $post_id )) ? set_post_format($post_id, 'gallery' ) : false;


            // MAILCHIMP

            if ( ! $wsc['mc-enable'] ) return;
            require_once dirname( __FILE__ ) . '/Mailchimp.php' ;
            $api_key = $wsc['mc-api']; 
            $list_id = $wsc['mc-listid']; 
            $umail = get_userdata( $user_id )->user_email;
            $Mailchimp = new Mailchimp( $api_key );
            $Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
            try {
                $subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => htmlentities( $umail ) ) );            
            } catch (Exception $e) { }

        }


        // =============================
        // showing the content of single submition
        // =============================
        public function showsall($id){
            global $post ;
            $terms = get_the_terms($post, 'ifcc_cats'); // pull cats from post
            if( !empty($terms) ) $term = array_pop($terms); // to string only one is possible cat!

            echo "<h1>".$term->name."</h1>";

            $fields = get_fields($post->ID);
            if( $fields ){
                foreach( $fields as $field_name => $value ){
                    $field = get_field_object($field_name, false, array('load_value' => false)); // var_dump($field); continue;

                    if( isset($value) && $value != '' ){
                        // if we are on picture field or tax field, skip it!
                        if( $field['type'] == 'image' ) continue;
                        if( $field['type'] == 'taxonomy' ) continue;

                        // format output
                        echo '<p>';
                            echo "<h5 key='".$field['key']. "'>" .$field['label']."</h5>";
                            if( ! is_array( $value ) ){ echo $value; }
                            if( $field['type'] == 'image' && $value ) echo '<img src="' . $value['url'] . '" />';
                            //  elseif ( $field['type'] == 'google_map' ){ print_r($value); }
                        echo '</p>';                        
                    }
                }
            }
        }


        // =============================
        // show contest categories in sidebar
        // =============================
        public function firstinsidebar( $name ) {
            global $post;
            global $wsc;

            if( $post == null ) return;
            if( ! $wsc['show-cats-in-sidebar'] ) return;

            if( $post->post_type == 'ifcc' ){
                echo '<div class="widget-area"><ul class="widget">';
                self::mk_custom_tax( 'ifcc_cats', isset( $wsc[ 'text-all-cats' ] ) ? $wsc[ 'text-all-cats' ] : 'All works' );
                echo '</ul></div>';
            }
        }

        /**
         * [mk_custom_tax description]
         * @param  [string] $taxstring [name of taxonomy, same that got registered by register_taxonomy]
         * @param  [string] $allword   [description of 'All items' like uncategorized category]
         * @return [string]            [list items of all items]
         */
        public function mk_custom_tax( $taxstring, $allword ){
            $terms = get_terms( $taxstring );
            $link = home_url( '/' ) . 'ifcc/?ifcc_cats=';
            $html = "<li><a href='". $link ."'>$allword</a></li>";    
            foreach($terms as $term) {
                $html .= "<li><a href='$link$term->slug'>$term->name</a></li>";
            }
            echo $html;
        }


        /**
         * Array of plugin arrays. Required keys are name and slug.
         * If the source is NOT from the .org repo, then source is also required.
         */
        public function my_plugin_register_required_plugins() {
            $plugins = array(
                // This is an example of how to include a plugin from the WordPress Plugin Repository.
                array(
                    'name'      => 'Advanced Custom Fields',
                    'slug'      => 'advanced-custom-fields',
                    'required'  => true,
                ),
                array(
                    'name'      => 'WooCommerce - excelling eCommerce',
                    'slug'      => 'woocommerce',
                    'required'  => true,
                ),
            );
            $config = array(
                'default_path' => '',                      // Default absolute path to pre-packaged plugins.
                'menu'         => 'ifcc-install-plugins', // Menu slug.
                'has_notices'  => true,                    // Show admin notices or not.
                'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
                'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
                'is_automatic' => false,                   // Automatically activate plugins after installation or not.
                'message'      => '',                      // Message to output right before the plugins table.
                'strings'      => array(
                    'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
                    'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
                    'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
                    'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
                    'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s).
                    'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s).
                    'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s).
                    'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
                    'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
                    'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s).
                    'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s).
                    'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s).
                    'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
                    'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins' ),
                    'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
                    'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
                    'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
                    'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
                )
            );
            tgmpa( $plugins, $config );
        }

    } // end class

}

// start our plugin
new woosimplecontest();
